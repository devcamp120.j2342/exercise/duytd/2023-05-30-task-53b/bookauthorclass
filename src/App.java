import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nguyen Truong Thanh", "thanh@gmail.com", 'f');
        System.out.println("Author 1");
        System.out.println(author1);

        Author author2 = new Author("Nguyen Quoc Thong", "thong@gmail.com", 'm');
        System.out.println("Author 2");
        System.out.println(author2);

        Book book1 = new Book("java", author1, 100000, 100);
        System.out.println("book 1");
        System.out.println(book1);
        
        Book book2 = new Book("React3s", author2, 200000, 0);
        System.out.println("book 2");
        System.out.println(book2);
    }
}
